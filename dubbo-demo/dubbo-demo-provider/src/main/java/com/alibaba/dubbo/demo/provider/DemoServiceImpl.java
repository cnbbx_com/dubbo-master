/*
 * Copyright 1999-2011 Alibaba Group.
 *  
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.alibaba.dubbo.demo.provider;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import com.alibaba.dubbo.demo.DemoService;
import com.alibaba.dubbo.rpc.RpcContext;

public class DemoServiceImpl implements DemoService {
    private String url = "jdbc:mysql://127.0.0.1:3306/dubbo";
    private String user = "root";
    private String password = "jinge520";

    public void Test(String name, String address) {
        Connection conn = null;
        PreparedStatement pstm = null;
        ResultSet rt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, password);
            String sql = "INSERT INTO userinfo(uname,uphone,uaddress) VALUES(?,?,?)";
            pstm = conn.prepareStatement(sql);
            Long startTime = System.currentTimeMillis();
            Random rand = new Random();
            int a, b, c, d;
            for (int i = 1; i <= 3; i++) {
                pstm.setString(1, name);
                a = rand.nextInt(10);
                b = rand.nextInt(10);
                c = rand.nextInt(10);
                d = rand.nextInt(10);
                pstm.setString(2, "188" + a + "88" + b + c + "66" + d);
                pstm.setString(3, address);
                pstm.executeUpdate();
            }
            pstm = conn.prepareStatement("INSERT INTO sumaddnum(uaddress,usum) VALUES(?,?)");
            pstm.setString(1, address);
            pstm.setInt(2, 3);
            pstm.executeUpdate();
            Long endTime = System.currentTimeMillis();
            System.out.println("OK,用时：" + (endTime - startTime));
        } catch (Exception e) {
            //e.printStackTrace();
            //throw new RuntimeException(e);
        } finally {
            if (pstm != null) {
                try {
                    pstm.close();
                } catch (SQLException e) {
                    //e.printStackTrace();
                    //throw new RuntimeException(e);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    //e.printStackTrace();
                    //throw new RuntimeException(e);
                }
            }
        }
    }

    public String sayHello(String name) {
        String RemoteAddress = RpcContext.getContext().getRemoteAddress().toString();
        Test(name, RemoteAddress);
        System.out.println("[" + new SimpleDateFormat("HH:mm:ss").format(new Date()) + "] Hello " + name + ", request from consumer: " + RemoteAddress);
        return "Hello " + name + ", response form provider: " + RpcContext.getContext().getLocalAddress();
    }

}